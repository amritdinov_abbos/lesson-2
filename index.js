const isDivisible = (val) => {
    if (val % 15 == 0) {
        console.log(val + " is divisible by 3 and 5")
    } else {
        console.log(val + " is not divisible by 3 and 5")
    }
}

const isEven = (val) => {
    if (val % 2 == 0) {
        console.log(val + " is Even")
    } else {
        console.log(val + " is Odd")
    }
}
const sortArray = (arr) => {
    console.log(arr.sort((a, b) => a - b)) 
}

const uniqueArr = (arr) => {
    let result = []
    for (let i = 0; i < arr.length; i++) {
        result = result.concat(arr[i].test)        
    }
    console.log([...new Set(result)])  
}
uniqueArr([
    {test: ['a', 'b', 'c', 'd']},
    {test: ['a', 'b', 'c']},
    {test: ['a', 'd']},
    {test: ['a', 'b', 'k', 'e', 'e']},
    ])